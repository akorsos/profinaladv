# proFinalAdv -- Alex Korsos #

### HonestY 0.8 Music Player ###

Originally this looked nothing like what it is now. It was supposed to be a two deck DJ player that used two JavaFX MediaPlayers and a crossfader. This smaller program developed after numerous effort failed to implement the larger one.

### Features ###

- Play/Pause Button
- Volume Slider
- Progress Display and Seeker
- Metadata Display
- Open File Browser Button for File Selection
- Drag and Drop for File Selection

### Things Still to be Implemented ###

The AudioWaveformCreator class in the project does work properly but I didn't have enough time today to add it into the project. It was implemented in the original project and was functioning but I had to literally make a new project and the waveform became a low priority. I would still like to implement them into the current project however.